/*
 * 4x4x4 LED Cube
 * Project by Andrew Rae
 */

#include "LEDCube.h"

/*
  Default Connection Setup:
  Columns
  [(x,y)-Pin]
  (0,0)-13
  (0,1)-12
  (0,2)-11
  (0,3)-10
  (1,0)-9
  (1,1)-8
  (1,2)-7
  (1,3)-6
  (2,0)-5
  (2-1)-4
  (2-2)-3
  (2,3)-2
  (3,0)-1
  (3,1)-0
  (3,2)-A5
  (3,3)-A4
  
  Layers
  [layer(z)-Pin]
  0-A0
  1-A1
  2-A2
  3-A3
*/

// coloumns unrolled (0,0) - (3,3)
int columns[16] = {13, 12, 11, 10,
                  9, 8, 7, 6,
                  5, 4, 3, 2,
                  0, 1, A5, A4};
// layers 0 - 3
int layers[4] = {A0, A1, A2, A3};
// this startes 
Cube cube(columns, layers);



/*--------------- Sample Animations ---------------*/
// the simple and but more tedious way
void spinningAnimation1(int cycles)
{
  // Create animation object and relevant frames
  Animation spin(&cube);
  spin.setFPS(10);
  Frame frame1;
  Frame frame2;
  Frame frame3;
  Frame frame4;
  Frame frame5;
  Frame frame6;

  // drawing lines in each frame
  frame1.drawLine(0,0,0, 3,3,0);
  frame1.drawLine(0,1,1, 3,2,1);
  frame1.drawLine(0,2,2, 3,1,2);
  frame1.drawLine(0,3,3, 3,0,3);

  frame2.drawLine(0,1,0, 3,2,0);
  frame2.drawLine(0,2,1, 3,1,1);
  frame2.drawLine(0,3,2, 3,0,2);
  frame2.drawLine(1,3,3, 2,0,3);
  
  frame3.drawLine(0,2,0, 3,1,0);
  frame3.drawLine(0,3,1, 3,0,1);
  frame3.drawLine(1,3,2, 2,0,2);
  frame3.drawLine(2,3,3, 1,0,3);
  
  frame4.drawLine(0,3,0, 3,0,0);
  frame4.drawLine(1,3,1, 2,0,1);
  frame4.drawLine(2,3,2, 1,0,2);
  frame4.drawLine(3,3,3, 0,0,3);
  
  frame5.drawLine(1,3,0, 2,0,0);
  frame5.drawLine(2,3,1, 1,0,1);
  frame5.drawLine(3,3,2, 0,0,2);
  frame5.drawLine(3,2,3, 0,1,3);
  
  frame6.drawLine(2,3,0, 1,0,0);
  frame6.drawLine(3,3,1, 0,0,1);
  frame6.drawLine(3,2,2, 0,1,2);
  frame6.drawLine(3,1,3, 0,2,3);

  // adding the frames to the spin animation object
  spin.addFrame(&frame1);
  spin.addFrame(&frame2);
  spin.addFrame(&frame3);
  spin.addFrame(&frame4);
  spin.addFrame(&frame5);
  spin.addFrame(&frame6);

  // run for the inputted amount of cycles
  spin.runCycles(cycles);
}

// the more complicated but more powerful way
void spinningAnimation2(int cycles)
{
  // Create animation object and relevant frames
  Animation spin(&cube);
  spin.setFPS(15);
  Frame frame1;
  Frame frame2;
  Frame frame3;
  Frame frame4;
  Frame frame5;
  Frame frame6;
  // this time we add blank frames first
  spin.addFrame(&frame1);
  spin.addFrame(&frame2);
  spin.addFrame(&frame3);
  spin.addFrame(&frame4);
  spin.addFrame(&frame5);
  spin.addFrame(&frame6);

  // then run a loop to generate all the frames appropriately
  Frame* curFrame; // this is used to acces frames
  int x = 0;
  int y = 0;
  for(int i = 0; i < 6; i++)
  {
    for(int z = 0; z < 4; z++)
    {
      curFrame = spin.getFrame();
      curFrame->drawLine(x,y,z, 3 - x,3 - y,z); // we use -> because curFrame is a pointer
    }
    for(int j = 0; j < 3; j++)
      curFrame = spin.getFrame();
      
    if(y < 3)
      y++;
    else
      x++;
  }

  // run for the inputted amount of cycles
  spin.runCycles(cycles);
}

// demonstration of copying and rotating
void diagonalWobble(int cycles)
{
  // Create animation object and relevant frames
  Animation diagonal(&cube);
  diagonal.setFPS(10);

  // create a base frame (diagonal plane in this case)
  Frame frame1;
  frame1.setLED(0,0,0,0);
  frame1.drawLine(1,0,0, 0,1,0);
  frame1.drawLine(2,0,1, 0,2,1);
  frame1.drawLine(3,0,1, 0,3,1);
  frame1.drawLine(3,1,2, 1,3,2);
  frame1.drawLine(3,2,2, 2,3,2);
  frame1.setLED(3,3,3,0);

  // copy frame 1 then rotate around the z axis
  Frame frame2;
  frame2.copyOf(&frame1);
  frame2.rotate(0, 0, 90);
  Frame frame3;
  frame3.copyOf(&frame1);
  frame3.rotate(0, 0, 180);
  Frame frame4;
  frame4.copyOf(&frame1);
  frame4.rotate(0, 0, 270);

  diagonal.addFrame(&frame1);
  diagonal.addFrame(&frame2);
  diagonal.addFrame(&frame3);
  diagonal.addFrame(&frame4);
  diagonal.runCycles(cycles);
}

/*--------------- Setup ---------------*/
void setup()
{
  // cube must be initialized
  cube.initializePins();
}

/*--------------- Main Loop ---------------*/
void loop()
{
 
  spinningAnimation2(100);
  diagonalWobble(100);
}