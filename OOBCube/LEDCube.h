/*
 * LEDCube.h
 * 4x4x4 LED Cube Classes
 * Project by Andrew Rae
 * April 2018
 */

#ifndef LEDCube_h
#define LEDCube_h

#include <stddef.h>
#include "Arduino.h"

 /*--------------- Cube Object ---------------*/
class Cube
{
  public:
    Cube(int columns[], int layers[]);
    ~Cube();
    void initializePins();

    /*--------------- Getters and Setters ---------------*/
    int* getColumns();
    void setColumns(int columns[]);
    int* getLayers();
    void setLayers(int layers[]);
    
  private:
    // pin layout for the columns
    int* _pcolumns = new int[4*4];
    int (&_columns)[4][4] = (int(&)[4][4])*this->_pcolumns;
    // pin layout for the layers
    int* _players = new int[4];
    int (&_layers)[4] = (int(&)[4])*this->_players;
};

/*--------------- Frame Object ---------------*/
class Frame
{
  public:
    Frame();
    ~Frame();

    /*--------------- Drawing Functions ---------------*/
    void turnAllOff();
    void turnAllOn();
    void drawLine(int x1, int y1, int z1, int x2, int y2, int z2, bool state = 0);
    void fill(int xDirection, int yDirection, int zDirection);
    void copyOf(Frame* srcFrame);
    void rotate(int xRot, int yRot, int zRot);

    /*--------------- Getters and Setters ---------------*/    
    bool* getLEDs();
    void setLED(int x, int y, int z, bool state);
    Frame* getPrev();
    void setPrev(Frame* prevFrame);
    Frame* getNext();
    void setNext(Frame* nextFrame);
    
  private:
    // array of all the LEDs locations
    bool* _pleds = new bool[4*4*4];
    bool (&_leds)[4][4][4] = (bool(&)[4][4][4])*this->_pleds;
    // previous and next frame for frame list
    Frame* _prevFrame = NULL;
    Frame* _nextFrame = NULL;
};

/*--------------- Frame List Object ---------------*/
class FrameList
{
  public:
    FrameList();
    ~FrameList();
    void add(Frame* frame);
    Frame* findFrame(int place);
    Frame* getFrame();
    void restart();
    
  private:
    int _curPlace = 0;
    int _length = 0;
    Frame* _head = NULL;
};

/*--------------- Animation Object ---------------*/
class Animation
{
  public:
    Animation(Cube* cube);
    ~Animation();
    void addFrame(Frame* frame);
    void turnEverythingOff();
    // draw current frame object
    void drawFrame();
    // run the animation for a duration of time (in seconds)
    void runDuration(int duration);
    // run through animation for x amount of cycles
    void runCycles(int cycles);
    void restart();

    /*--------------- Getters and Setters ---------------*/
    void setFPS(int fps);
    Frame* getFrame();
    
  private:
    int _numFrames = 0;
    int _fps = 10;
    Cube* _cube;
    FrameList _frameList;
    Frame* _curFrame = NULL;
};

#endif