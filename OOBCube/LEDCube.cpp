/*
 * LEDCube.cpp
 * 4x4x4 LED Cube Methods
 * Project by Andrew Rae
 * April 2018
 */

#include <stddef.h>
#include <math.h>

#include "Arduino.h"
#include "LEDCube.h"

/*--------------- Cube Methods ---------------*/
Cube::Cube(int columns[], int layers[])
{
  this->setColumns(columns);
  this->setLayers(layers);
}

Cube::~Cube()
{
  delete[] this->_pcolumns;
  delete[] this->_layers;
}

void Cube::initializePins()
{
  // initialize column pins
  for (int x = 0; x < 4; x++)
    for (int y = 0; y < 4; y++)
      pinMode(this->_columns[x][y], OUTPUT);
      
  // initial layer pins
  for (int z = 0; z < 4; z++)
    pinMode(this->_layers[z], OUTPUT);
}

/*--------------- Cube Getters and Setters ---------------*/
int* Cube::getColumns()
{
  return this->_pcolumns;
}

void Cube::setColumns(int columns[])
{
  int i = 0;
  for(int y = 0; y < 4; y++)
    for(int x = 0; x < 4; x++)
    {
      this->_columns[x][y] = columns[i];
      i++;
    }
}

int* Cube::getLayers()
{
  return this->_players;
}

void Cube::setLayers(int layers[])
{
  for(int z = 0; z < 4; z++)
    this->_layers[z] = layers[z];
}

/*--------------- Frame Methods ---------------*/
Frame::Frame()
{
  this->turnAllOff();
}

Frame::~Frame()
{
  delete[] this->_pleds;
}

/*--------------- Frame Drawing Functions ---------------*/
void Frame::turnAllOff()
{
  for(int x = 0; x < 4; x++)
  {
    for(int y = 0; y < 4; y++)
    {
      for(int z = 0; z < 4; z++)
      {
        // 1 is technically "off" the way my cube is wired, depends on the setup
        this->setLED(x, y, z, 1);
      }
    }
  }
}

void Frame::turnAllOn()
{
  for(int x = 0; x < 4; x++)
  {
    for(int y = 0; y < 4; y++)
    {
      for(int z = 0; z < 4; z++)
      {
        this->setLED(x, y, z, 0);
      }
    }
  }
}

void Frame::drawLine(int x1, int y1, int z1, int x2, int y2, int z2, bool state = 0)
{
  int xDiff = x2 - x1;
  int yDiff = y2 - y1;
  int zDiff = z2 - z1;
  int xDirection = xDiff < 0 ? -1: 1;
  int yDirection = yDiff < 0 ? -1: 1;
  int zDirection = zDiff < 0 ? -1: 1;
  // find greatest difference
  int greatestDiff = abs(xDiff);
  if(abs(yDiff) > greatestDiff)
    greatestDiff = abs(yDiff);
  if(abs(zDiff) > greatestDiff)
    greatestDiff = abs(zDiff);
  // increment each x,y,z value based on the line
  float increment = 1.0 / greatestDiff;
  float x = x1;
  float y = y1;
  float z = z1;
  while( (x*xDirection <= x2*xDirection) && (xDiff != 0)
       || (y*yDirection <= y2*yDirection) && (yDiff != 0)
       || (z*zDirection <= z2*zDirection) && (zDiff != 0))
  {
    // set nearest LED to on (may do it more than once to same LED but that's ok)
    this->setLED(round(x), round(y), round(z), state);
    // increment each x,y,z in appropriate direction
    x += increment * xDiff;
    y += increment * yDiff;
    z += increment * zDiff;
  }
}

void Frame::fill(int xDirection, int yDirection, int zDirection)
{
  bool foundLED = false;
  if (xDirection)
  {
    for(int y = 0; y < 4; y++)
      for(int z = 0; z < 4; z++)
      {
        foundLED = false;
        for(int x = 0; x < 4; x++)
        {
          if(xDirection > 0)
          {
            if(foundLED)
              this->setLED(x, y, z, 0);
            else if(this->_leds[x][y][z] == 0)
              foundLED = true;
          }
          else
          {
            if(foundLED)
              this->setLED(3 - x, y, z, 0);
            else if(this->_leds[3 - x][y][z] == 0)
              foundLED = true; 
          }
        }
      }
  }

  if (yDirection)
  {
    for(int x = 0; x < 4; x++)
      for(int z = 0; z < 4; z++)
      {
        foundLED = false;
        for(int y = 0; y < 4; y++)
        {
          if(yDirection > 0)
          {
            if(foundLED)
              this->setLED(x, y, z, 0);
            else if(this->_leds[x][y][z] == 0)
              foundLED = true;
          }
          else
          {
            if(foundLED)
              this->setLED(x, 3 - y, z, 0);
            else if(this->_leds[x][3 - y][z] == 0)
              foundLED = true; 
          }
        }
      }
  }

  if (zDirection)
  {
    for(int x = 0; x < 4; x++)
      for(int y = 0; y < 4; y++)
      {
        foundLED = false;
        for(int z = 0; z < 4; z++)
        {
          if(zDirection > 0)
          {
            if(foundLED)
              this->setLED(x, y, z, 0);
            else if(this->_leds[x][y][z] == 0)
              foundLED = true;
          }
          else
          {
            if(foundLED)
              this->setLED(x, y, 3 - z, 0);
            else if(this->_leds[x][y][3 - z] == 0)
              foundLED = true; 
          }
        }
      }
  }
}

void Frame::copyOf(Frame* srcFrame)
{
  // get the source led arrays
  bool* pSrcLeds = srcFrame->getLEDs();
  bool (&srcLeds)[4][4][4] = (bool(&)[4][4][4])*pSrcLeds;
  // deep copy to this frame
  for(int x = 0; x < 4; x++)
    for(int y = 0; y < 4; y++)
      for(int z = 0; z < 4; z++)
        this->setLED(x,y,z, srcLeds[x][y][z]);
}

// rotate pixels in frame along an axis (only takes on axis at a time)
void Frame::rotate(int xRot, int yRot, int zRot)
{
  double tempX;
  double tempY;
  double tempZ;

  // make a deep copy of the leds
  bool ledCopy[4][4][4];
  for(int x = 0; x < 4; x++)
    for(int y = 0; y < 4; y++)
      for(int z = 0; z < 4; z++)
        ledCopy[x][y][z] = this->_leds[x][y][z];

  // if rotating along x
  if(xRot)
  {
    double xRad = xRot * M_PI / 180.0;
    double vCos = cos(xRad);
    double vSin = sin(xRad);
    for(int x = 0; x < 4; x++)
    {
      for(int y = 0; y < 4; y++)
      {
        for(int z = 0; z < 4; z++)
        {
          tempY = ((y - 1.5)*vCos - (z - 1.5)*vSin) + 1.5;
          tempZ = ((y - 1.5)*vSin + (z - 1.5)*vCos) + 1.5;
          this->setLED(x, round(tempY), round(tempZ), ledCopy[x][y][z]);
        }
      }
    }
    for(int x = 0; x < 4; x++)
      for(int y = 0; y < 4; y++)
        for(int z = 0; z < 4; z++)
          ledCopy[x][y][z] = this->_leds[x][y][z];
  }

  // if rotating along y
  if(yRot)
  {
    double yRad = yRot * M_PI / 180.0;
    double vCos = cos(yRad);
    double vSin = sin(yRad);
    for(int x = 0; x < 4; x++)
    {
      for(int y = 0; y < 4; y++)
      {
        for(int z = 0; z < 4; z++)
        {
          tempX = ((x - 1.5)*vCos - (z - 1.5)*vSin) + 1.5;
          tempZ = ((x - 1.5)*vSin + (z - 1.5)*vCos) + 1.5;
          this->setLED(round(tempX), y, round(tempZ), ledCopy[x][y][z]);
        }
      }
    }
    for(int x = 0; x < 4; x++)
      for(int y = 0; y < 4; y++)
        for(int z = 0; z < 4; z++)
          ledCopy[x][y][z] = this->_leds[x][y][z];
  }

  // if rotating along y
  if(zRot)
  {
    double zRad = (double)zRot * M_PI / 180.0;
    double vCos = cos(zRad);
    double vSin = sin(zRad);
    for(int x = 0; x < 4; x++)
    {
      for(int y = 0; y < 4; y++)
      {
        for(int z = 0; z < 4; z++)
        {
          tempX = ((x - 1.5)*vCos - (y - 1.5)*vSin) + 1.5;
          tempY = ((x - 1.5)*vSin + (y - 1.5)*vCos) + 1.5;
          this->setLED(round(tempX), round(tempY), z, ledCopy[x][y][z]);
        }
      }
    }
  }
}

/*--------------- Frame Getters and Setters ---------------*/    
bool* Frame::getLEDs()
{
  return this->_pleds;
}

void Frame::setLED(int x, int y, int z, bool state)
{
  this->_leds[x][y][z] = state;
}

Frame* Frame::getPrev()
{
  return this->_prevFrame;
}

void Frame::setPrev(Frame* prevFrame)
{
  this->_prevFrame = prevFrame;
}

Frame* Frame::getNext()
{
  return this->_nextFrame;
}

void Frame::setNext(Frame* nextFrame)
{
  this->_nextFrame = nextFrame;
}

/*--------------- Frame List Methods ---------------*/
FrameList::FrameList(){}

FrameList::~FrameList(){}

void FrameList::add(Frame* frame)
{
  if(this->_length == 0)
    this->_head = frame;
  else
  {
    //find what is currently the last frame and add to it
    Frame* lastFrame = this->findFrame(this->_length);
    lastFrame->setNext(frame);
    frame->setPrev(lastFrame);
  }
  this->_length++;
}

Frame* FrameList::findFrame(int place)
{
  Frame* frame = this->_head;
  for(int i = 1; i < place; i++)
  {
    frame = frame->getNext();
  }
  return frame;
}

Frame* FrameList::getFrame()
{
  if (this->_length == 0)
    return NULL;
  else
  {
    this->_curPlace++;
    if(this->_curPlace > this->_length)
      _curPlace = 1;
    return this->findFrame(this->_curPlace);
  }
}

void FrameList::restart()
{
  this->_curPlace = 0;
}

/*--------------- Animation Methods ---------------*/
Animation::Animation(Cube* cube)
{
  this->_cube = cube;
}

Animation::~Animation() {}

void Animation::addFrame(Frame* frame)
{
  this->_numFrames++;
  this->_frameList.add(frame);
}

void Animation::turnEverythingOff()
{
  int* pcolumns = this->_cube->getColumns();
  int (&columns)[4][4] = (int(&)[4][4])*pcolumns;
  int* players = this->_cube->getLayers();
  int (&layers)[4] = (int(&)[4])*players;
  for(int x = 0; x < 4; x++)
    for(int y = 0; y < 4; y++)
      digitalWrite(columns[x][y], 1);
  for(int z = 0; z < 4; z++)
    digitalWrite(layers[z], 0);
}

// draw current frame object
void Animation::drawFrame()
{
  // get all the led arrays
  bool* pleds = this->_curFrame->getLEDs();
  bool (&leds)[4][4][4] = (bool(&)[4][4][4])*pleds;
  int* pcolumns = this->_cube->getColumns();
  int (&columns)[4][4] = (int(&)[4][4])*pcolumns;
  int* players = this->_cube->getLayers();
  int (&layers)[4] = (int(&)[4])*players;
  
  // calculate duration of a frame in millis
  int duration = 1000 / this->_fps / 4;
  // variables for exact milli timer
  unsigned long curMillis = millis();
  unsigned long startMillis = curMillis;
  for(int t = 0; t < duration; t++)
  {
    // multiplexer
    // cycles through layers each millisecond in order to show any LED pattern
    for(int z = 0; z < 4; z++)
    {
      // set start time
      startMillis = millis();
      
      // turn off all LEDs
      this->turnEverythingOff();
      // turn on current layer
      digitalWrite(layers[z], 1);
      
      
      // writing to LEDs
      for(int x = 0; x < 4; x++)
      {
        for(int y = 0; y < 4; y++)
        {
          digitalWrite(columns[x][y], leds[x][y][z]);
        }
        
      }
      
      // makes sure each layer cycles takes exactly one millisecond
      // (should be more consistent than delay(1))
      curMillis = millis();
      while(curMillis - startMillis < 1)
      {
        curMillis = millis();
      }
    }
  }
}

// run the animation for a duration of time (in seconds)
void Animation::runDuration(int duration)
{
  
}

// run through animation for x amount of cycles
void Animation::runCycles(int cycles)
{
  // restart in case frame list was in progress
  this->_frameList.restart();
  for(int i = 0; i < (cycles * this->_numFrames); i++)
  {
    // gets next frame
    this->_curFrame = this->_frameList.getFrame();
    this->drawFrame();
  }
  this->_frameList.restart();
}

void Animation::restart()
{
  this->_frameList.restart();
}

/*--------------- Animation Getters and Setters ---------------*/

void Animation::setFPS(int fps)
{
  this->_fps = fps;
}

Frame* Animation::getFrame()
{
  return this->_frameList.getFrame();
}
