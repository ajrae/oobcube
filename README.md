# OOBCube
Object Oriented approach to 4x4x4 LED cube. Using provided classes it is easy to create a multiplexed animation.

An example of what's possible with this library:
![Alt Text](https://i.imgur.com/6Gdc4LW.gif)
